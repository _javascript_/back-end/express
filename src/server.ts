import http from "http";
import https from "https";
import fs from "fs";

import express, { Express, Request, Response } from "express";
import cors from "cors";
import cookieParser from "cookie-parser";

import { router } from "./router";
import { errorMiddleware } from "./middlewares";
import { config } from "./config";

export class Server {
  static createServer() {
    const key = fs.readFileSync(config.paths.cert.key, "utf8");
    const cert = fs.readFileSync(config.paths.cert.cert, "utf8");
    const options = key && cert ? { key, cert } : null;

    const app: Express = express();
    app.use(express.json());
    app.use(
      cors({
        credentials: true,
        origin: config.clientUrl,
      })
    );
    app.use(cookieParser());
    app.use("/api/v1", router);
    app.use(errorMiddleware);

    app.get("/", (req: Request, res: Response) => {
      res.send("Express + TypeScript Server!!!");
    });

    const server = options
      ? https.createServer(options, app)
      : http.createServer(app);
    return server;
  }
}
