import { hash, compare } from "bcrypt";

class HashService {
  async hashing(value: string) {
    return await hash(value, 3);
  }
  async compare(data: string | Buffer, encrypted: string) {
    return await compare(data, encrypted);
  }
}

export const hashService = new HashService();
