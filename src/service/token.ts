import { sign, verify } from "jsonwebtoken";
import { IToken } from "../models/Token";
import { config } from "../config";
import { tokenRepository } from "../repositories";
import { IUser } from "../models/User";

class TokenService {
  async generateToken(payload: string | object | Buffer) {
    const accessToken = sign(payload, config.jwt.access, { expiresIn: "10s" });
    const refreshToken = sign(payload, config.jwt.refresh, {
      expiresIn: "30d",
    });
    return { accessToken, refreshToken };
  }

  async saveToken(userId: number, refreshToken: string) {
    const currentToken = await tokenRepository.getOneByUserId(userId);
    if (currentToken) {
      return await tokenRepository.update({
        tokenId: currentToken.tokenId,
        refreshToken,
      });
    }
    const token = await tokenRepository.create({ refreshToken }, userId);

    return token as IToken;
  }

  async removeToken(refreshToken: string) {
    return await tokenRepository.delete({ refreshToken });
  }

  validateRefreshToken(token: string) {
    try {
      return verify(token, config.jwt.refresh) as IUser;
    } catch (error) {
      return null;
    }
  }
  validateAccessToken(token: string) {
    try {
      return verify(token, config.jwt.access) as IUser;
    } catch (error) {
      return null;
    }
  }

  async getOneByRefreshToken(refreshToken: string) {
    return await tokenRepository.getOne({ refreshToken });
  }
}

export const tokenService = new TokenService();
