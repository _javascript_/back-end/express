import { createTransport, Transporter } from "nodemailer";
import SMTPTransport from "nodemailer/lib/smtp-transport";
import { logger } from "../utils";
import { config } from "../config";

class MailService {
  transporter: Transporter<SMTPTransport.SentMessageInfo> | null = null;
  constructor() {
    try {
      this.transporter = createTransport({
        ...config.smtp,
      });
    } catch (error: any) {
      throw new Error("[MailService]: constructor -> " + error.message);
    }
  }
  async sendActivationMail(to: string, link: string) {
    try {
      if (!this.transporter) {
        return console.log(`Send email to: ${to}, link: ${link}`);
      }
      await this.transporter.sendMail({
        from: config.smtp.auth.user,
        to,
        subject: `Активация аккаунта на ${config.apiUrl}`,
        text: "",
        html: `
          <div>
            <h1>Для активации перейдите по ссылке</h1>
            <a href="${link}">${link}</a>
          </div>
        `,
      });
    } catch (error: any) {
      const msg = "[MailService]: sendActivationMail -> " + error.message;
      logger.error(msg);
      throw new Error(msg);
    }
  }
}

export const mailService = new MailService();
