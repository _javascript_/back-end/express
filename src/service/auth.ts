import { v4 as uuid } from "uuid";

import { userRepository } from "../repositories";
import { config } from "../config";
import { UserResponseDTO } from "../models/User";
import { ApiError } from "../service";

import { hashService } from "./hash";
import { mailService } from "./mail";
import { tokenService } from "./token";

function validateEmail(email: string) {
  var pattern =
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return pattern.test(email);
}

class AuthService {
  async registration(email: string, password: string) {
    if (!validateEmail(email))
      throw ApiError.BadRequest(`Не коректный email: ${email}`);
    const candidate = await userRepository.getOne({ email });
    if (candidate) {
      throw ApiError.BadRequest(
        `Пользователь с email: "${email}" уже существует`
      );
    }
    const hashPassword = await hashService.hashing(password);
    const activationLink = uuid();
    const user = await userRepository.create({
      email,
      password: hashPassword,
      activationLink,
    });
    if (!user) return;
    try {
      await mailService.sendActivationMail(
        email,
        `${config.apiUrl}/api/v1/auth/activate/${activationLink}`
      );
    } catch (error: any) {
      console.log(error.message);
    }
    const responseUser = new UserResponseDTO(user);
    const tokens = await tokenService.generateToken({ ...responseUser });
    try {
      await tokenService.saveToken(user.userId, tokens.refreshToken);
    } catch (error: any) {
      throw new Error(error.message);
    }
    return { tokens, user: responseUser };
  }

  async activate(activationLink: string) {
    try {
      const user = await userRepository.getOne({ activationLink });
      if (!user) throw ApiError.BadRequest("Неккоректная ссылка активации");
      if (user.isActivated)
        throw ApiError.BadRequest("Пользователь активирован");
      await userRepository.update(
        { isActivated: true },
        { userId: user.userId }
      );
    } catch (error: any) {
      throw new Error(`[AuthService]: activate -> ` + error.message);
    }
  }

  async signIn(email: string, password: string) {
    const candidate = await userRepository.getOne({ email });
    if (!candidate) {
      throw ApiError.BadRequest(`Пользователь с email: "${email}" не найден`);
    }
    if (!candidate.isActivated) {
      throw ApiError.BadRequest("Пользователь не активирован");
    }
    const isPassEquals = await hashService.compare(
      password,
      candidate.password
    );
    if (!isPassEquals) {
      throw ApiError.BadRequest(`Не верный пароль`);
    }
    const userDto = new UserResponseDTO(candidate);
    const tokens = await tokenService.generateToken({ ...userDto });
    await tokenService.saveToken(userDto.userId, tokens.refreshToken);
    return { tokens, user: userDto };
  }

  async signOut(refreshToken: string) {
    return await tokenService.removeToken(refreshToken);
  }

  async refresh(refreshToken: string) {
    if (!refreshToken) {
      throw ApiError.UnauthorizedError();
    }
    const userData = tokenService.validateRefreshToken(refreshToken);
    const tokenFromDb = await tokenService.getOneByRefreshToken(refreshToken);
    console.log({ userData, tokenFromDb });
    if (!userData || !tokenFromDb) {
      throw ApiError.UnauthorizedError();
    }
    const user = await userRepository.getOne({ userId: userData.userId });
    if (!user) return null;
    const userDto = new UserResponseDTO(user);
    const tokens = await tokenService.generateToken({ ...userDto });
    await tokenService.saveToken(user.userId, tokens.refreshToken);
    return { tokens, user: userDto };
  }
}

export const authService = new AuthService();
