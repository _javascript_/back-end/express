import { userRepository } from "../repositories";

class UserService {
  async getAll() {
    return await userRepository.get();
  }
}

export const userService = new UserService();
