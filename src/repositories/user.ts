import { IUser } from "../models/User";
import { db, objectSnakeCaseToCamelCase, camelCaseToSnakeCase } from "../utils";

import { tokenRepository } from "./token";

function getParams(params: object) {
  return Object.entries(params)
    .map(([key, value]) => {
      return `${camelCaseToSnakeCase(key)} = '${value}'`;
    })
    .join(", ");
}

function getLike(params: object) {
  return Object.entries(params)
    .map(([key, value]) => {
      return `${camelCaseToSnakeCase(key)} LIKE '%${value}%'`;
    })
    .join(", ");
}

class UserRepository {
  async get(query?: Partial<IUser>) {
    const where = query ? `WHERE ${getLike(query)}` : "";
    const sql = `SELECT * FROM public.user ${where}`;
    const result = await db.query(sql);
    if (!result.rows.length) return null;
    return objectSnakeCaseToCamelCase(result.rows) as IUser[];
  }
  async getByRefreshToken(refreshToken: string) {
    const token = await tokenRepository.getOne({ refreshToken });
    console.log({ token });
    if (!token) return null;
    const sql = `select * from user_token join "user" on user_token.user_id = "user".user_id where user_token.token_id = ${token.tokenId}`;
    const result = await db.query(sql);
    if (!result.rows.length) return null;
    return objectSnakeCaseToCamelCase(result.rows[0]) as IUser;
  }
  async getOne(params: Partial<IUser> = {}) {
    const args = Object.entries(params)
      .map(([key, value]) => {
        return `${camelCaseToSnakeCase(key)} = '${value}'`;
      })
      .join(", ");
    const sql = `SELECT * FROM public.user WHERE ${args}`;
    const result = await db.query(sql);
    if (!result.rows.length) return null;
    return objectSnakeCaseToCamelCase(result.rows[0]) as IUser;
  }
  async delete() {}
  async create(user: Pick<IUser, "email" | "password" | "activationLink">) {
    const sql = `
      INSERT INTO public."user"
        (email, "password", is_activated, activation_link)
        VALUES('${user.email}', '${user.password}', false, '${user.activationLink}')
        returning *
      `;
    const result = await db.query(sql);
    if (!result.rows.length) return null;
    return objectSnakeCaseToCamelCase(result.rows[0]) as IUser;
  }
  async update(params: Partial<IUser>, where: Partial<IUser>) {
    const sql = `
      UPDATE public."user"
      SET ${getParams(params)}
      WHERE ${getParams(where)}
      returning *
    `;
    const result = await db.query(sql);
    if (!result.rows.length) return null;
    return objectSnakeCaseToCamelCase(result.rows[0]) as IUser;
  }
}

export const userRepository = new UserRepository();
