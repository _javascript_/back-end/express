import { db } from "../utils";
import { IToken } from "../models/Token";
import { camelCaseToSnakeCase, objectSnakeCaseToCamelCase } from "../utils";

class TokenRepository {
  async getOneByUserId(userId: number): Promise<IToken | null> {
    const sql = `
      select * from user_token join "token" on user_token.token_id = "token".token_id where user_token.user_id = ${userId}
    `;
    const result = await db.query(sql);
    if (!result.rows.length) return null;
    return objectSnakeCaseToCamelCase(result.rows[0]) as IToken;
  }
  async getOne(params: Partial<IToken>) {
    const args = Object.entries(params)
      .map(([key, value]) => {
        return `${camelCaseToSnakeCase(key)} = '${value}'`;
      })
      .join(", ");
    const sql = `SELECT * FROM public.token WHERE ${args}`;
    const result = await db.query(sql);
    if (!result.rows.length) return null;
    return objectSnakeCaseToCamelCase(result.rows[0]) as IToken;
  }
  async update(params: IToken) {
    const sql = `
      UPDATE public."token"
      SET refresh_token='${params.refreshToken}'
      WHERE token_id=${params.tokenId}
    `;
    const result = await db.query(sql);
    if (!result.rows.length) return null;
    return objectSnakeCaseToCamelCase(result.rows[0]);
  }
  async delete(params: Partial<IToken>) {
    const args = Object.entries(params)
      .map(([key, value]) => {
        return `${camelCaseToSnakeCase(key)} = '${value}'`;
      })
      .join(", ");
    const sql = `
      DELETE FROM public."token"
      WHERE ${args}
      returning *
    `;
    const result = await db.query(sql);
    if (!result.rows.length) return null;
    return objectSnakeCaseToCamelCase(result.rows[0]) as IToken;
  }
  async create(dto: Pick<IToken, "refreshToken">, userId: number) {
    let sql = `
      INSERT INTO public."token"
      (refresh_token)
      VALUES('${dto.refreshToken}')
      returning *
    `;
    let result = await db.query(sql);
    if (!result.rows.length) return null;
    const token = objectSnakeCaseToCamelCase(result.rows[0]) as IToken;
    sql = `
      INSERT INTO public.user_token
      (user_id, token_id)
      VALUES(${userId}, ${token.tokenId});
    `;
    await db.query(sql);
    return token;
  }
}

export const tokenRepository = new TokenRepository();
