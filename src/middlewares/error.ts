import { Request, Response, NextFunction } from "express";
import { ApiError } from "../service/error";
import { logger } from "../utils";

export function errorMiddleware(
  err: ApiError,
  req: Request,
  res: Response,
  next: NextFunction
) {
  logger.error(err);

  if (err instanceof ApiError) {
    return res
      .status(err.status)
      .json({ message: err.message, errors: err.errors });
  }
  return res.status(500).json({ message: "Server Error" });
}
