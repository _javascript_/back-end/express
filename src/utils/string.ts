export function capitalizeFirstLetter(string: string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export const camelCaseToSnakeCase = (string: string) => {
  return string
    .replace(/\W+/g, " ")
    .split(/ |\B(?=[A-Z])/)
    .map((word) => word.toLowerCase())
    .join("_");
};

export const snakeCaseToCamelCase = (string: string) => {
  return string
    .split("_")
    .map((word, index) => {
      if (index === 0) return word;
      return capitalizeFirstLetter(word);
    })
    .join("");
};

export const objectSnakeCaseToCamelCase = <T>(obj: object): T => {
  return Object.entries(obj).reduce<any>((acc, [key, value]) => {
    acc[snakeCaseToCamelCase(key)] = value;
    return acc;
  }, {});
};
