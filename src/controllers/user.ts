import { NextFunction, Request, Response } from "express";
import { userService } from "../service";

class UserController {
  async getAll(req: Request, res: Response, next: NextFunction) {
    const result = await userService.getAll();
    res.json(result);
  }
}

export const userController = new UserController();
