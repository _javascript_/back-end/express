import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";

import { authService, ApiError } from "../service";
import { config } from "../config";

const DAY_30 = 30 * 24 * 60 * 1000;

class AuthController {
  async registration(req: Request, res: Response, next: NextFunction) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        throw ApiError.BadRequest("Ошибка при валидации", errors.array());
      }
      const { email, password } = req.body;
      const result = await authService.registration(email, password);
      if (!result) throw new Error("Нэт результа");
      res.cookie("refreshToken", result.tokens.refreshToken, {
        maxAge: DAY_30,
        httpOnly: true,
        secure: true,
        sameSite: "none",
      });
      res.json({ accessToken: result.tokens.accessToken });
    } catch (error) {
      next(error);
    }
  }
  async signIn(req: Request, res: Response, next: NextFunction) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        throw ApiError.BadRequest("Ошибка при валидации", errors.array());
      }
      const { email, password } = req.body;

      const result = await authService.signIn(email, password);
      if (!result) throw new Error("Нэт результа");
      res.cookie("refreshToken", result.tokens.refreshToken, {
        maxAge: DAY_30,
        httpOnly: true,
        secure: true,
        sameSite: "none",
      });
      res.json({ accessToken: result.tokens.accessToken });
    } catch (error) {
      next(error);
    }
  }
  async signOut(req: Request, res: Response, next: NextFunction) {
    try {
      const { refreshToken } = req.cookies;
      if (!refreshToken) {
        throw ApiError.BadRequest("Ты не авторизован");
      }
      const result = await authService.signOut(refreshToken);
      res.clearCookie("refreshToken");
      res.json(result);
    } catch (error) {
      next(error);
    }
  }
  async refresh(req: Request, res: Response, next: NextFunction) {
    try {
      const { refreshToken } = req.cookies;
      if (!refreshToken) {
        throw ApiError.UnauthorizedError();
      }
      const result = await authService.refresh(refreshToken);
      if (!result) throw new Error("Нэт результа");
      res.cookie("refreshToken", result.tokens.refreshToken, {
        maxAge: DAY_30,
        httpOnly: true,
        secure: true,
        sameSite: "none",
      });
      res.json({ accessToken: result.tokens.accessToken });
    } catch (error) {
      next(error);
    }
  }
  async activate(req: Request, res: Response, next: NextFunction) {
    try {
      const activationLink = req.params.link;
      if (!activationLink) throw new Error("Not found activation link");
      await authService.activate(activationLink);
      return res.redirect(config.clientUrl);
    } catch (error: any) {
      next(error);
    }
  }
}

export const authController = new AuthController();
