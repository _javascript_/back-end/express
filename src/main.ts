import { Server } from "./server";
import { config } from "./config";
import { logger } from "./utils";

const server = Server.createServer();

async function bootstrap() {
  try {
    server.listen(config.port, () => {
      logger.info(
        `⚡️[server]: Server is running at https://localhost:${config.port}`
      );
    });
  } catch (error) {
    logger.error(error);
  }
}
bootstrap();
