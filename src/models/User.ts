export type IUser = {
  userId: number;
  email: string;
  password: string;
  isActivated: boolean;
  activationLink: string;
};

export class UserResponseDTO {
  userId: number;
  email: string;
  isActivated: boolean;
  activationLink: string;

  constructor(user: IUser) {
    this.userId = user.userId;
    this.email = user.email;
    this.isActivated = user.isActivated;
    this.activationLink = user.activationLink;
  }
}
