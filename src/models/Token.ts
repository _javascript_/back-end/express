import { IUser } from "./User";

export type IToken = {
  tokenId: number;
  refreshToken: string;
};
