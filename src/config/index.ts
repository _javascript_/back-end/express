import dotenv from "dotenv";
import path from "path";

dotenv.config();

const ROOT = process.cwd();

class Config {
  apiUrl: string;
  clientUrl: string;
  port: number;
  paths: {
    root: string;
    cert: {
      key: string;
      cert: string;
    };
  };
  db: {
    user: string;
    host: string;
    database: string;
    password: string;
    port: number;
  };
  jwt: {
    access: string;
    refresh: string;
  };
  smtp: {
    host: string;
    port: number;
    secure: boolean;
    auth: {
      user: string;
      pass: string;
    };
  };

  constructor() {
    this.port = Number(process.env.PORT) ?? 5000;
    this.paths = {
      root: ROOT,
      cert: {
        key: path.join(ROOT, "cert", "key.pem"),
        cert: path.join(ROOT, "cert", "cert.pem"),
      },
    };
    this.db = {
      user: process.env.POSTGRES_USER ?? "dima",
      database: process.env.POSTGRES_DB ?? "application",
      password: process.env.POSTGRES_PASSWORD ?? "password-dima",
      port: 5432,
      host: "localhost",
    };
    this.jwt = {
      access: process.env.JWT_ACCESS_SECRET ?? "secret-key",
      refresh: process.env.JWT_REFRESH_SECRET ?? "secret-key",
    };
    this.smtp = {
      host: process.env.SMTP_HOST ?? "",
      port: Number(process.env.SMTP_PORT) ?? 0,
      secure: false,
      auth: {
        user: process.env.SMTP_USER ?? "",
        pass: process.env.SMTP_PASSWORD ?? "",
      },
    };
    this.apiUrl = process.env.API_URL ?? "";
    this.clientUrl = process.env.CLIENT_URL ?? "";
  }
}

export const config = new Config();
