import { Router } from "express";
import { userController } from "../controllers";

import { authMiddleware } from "../middlewares";

const userRouter = Router();

userRouter.get("/", authMiddleware, userController.getAll);

export { userRouter };
