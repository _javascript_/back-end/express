import { body } from "express-validator";

import { Router } from "express";
import { authController } from "../controllers";

const authRouter = Router();

authRouter.post(
  "/sign-in",
  body("email").isEmail(),
  body("password").isLength({ min: 3, max: 32 }),
  authController.signIn
);
authRouter.post(
  "/registration",
  body("email").isEmail(),
  body("password").isLength({ min: 3, max: 32 }),
  authController.registration
);
authRouter.get("/sign-out", authController.signOut);
authRouter.get("/refresh", authController.refresh);
authRouter.get("/activate/:link", authController.activate);

export { authRouter };
