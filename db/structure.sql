DROP TABLE IF EXISTS "user" cascade;

CREATE TABLE "user" (
    "user_id" bigint generated always as identity,
    "email" varchar unique NOT null,
    "password" varchar NOT null,
    "is_activated" boolean default false,
    "activation_link" varchar
);
ALTER TABLE "user" ADD CONSTRAINT "pk_user" PRIMARY KEY ("user_id");

DROP TABLE IF EXISTS "token" cascade;

CREATE TABLE "token" (
    "token_id" bigint generated always as identity,
	"refresh_token" varchar not null
);

ALTER TABLE "token" ADD CONSTRAINT "pk_token" PRIMARY KEY ("token_id");

DROP TABLE IF EXISTS "user_token" cascade;

CREATE TABLE "user_token" (
    "user_id" bigint NOT NULL,
    "token_id" bigint NOT NULL,
    "created_at" timestamp NOT NULL DEFAULT current_timestamp,
    "updated_at" timestamp NOT NULL DEFAULT current_timestamp
);

ALTER TABLE "user_token" ADD CONSTRAINT "pk_user_token" PRIMARY KEY ("user_id", "token_id");
ALTER TABLE "user_token" ADD CONSTRAINT "fk_user_token_account" FOREIGN KEY ("user_id") REFERENCES "user" ("user_id") ON DELETE CASCADE;
ALTER TABLE "user_token" ADD CONSTRAINT "fk_user_token_token" FOREIGN KEY ("token_id") REFERENCES "token" ("token_id") ON DELETE CASCADE;